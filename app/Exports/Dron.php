<?php

namespace App\Exports;

use App\Hdron;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;


class dron implements FromView
{
    public function view(): View
    {
        return view('exportar.dron', [
            'query_dron' => Hdron::all()
        ]);
    }
}