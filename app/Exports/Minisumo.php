<?php

namespace App\Exports;

use App\Hminisumo;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;


class minisumo implements FromView
{
    public function view(): View
    {
        return view('exportar.minisumo', [
            'query_minisumo' => Hminisumo::all()
        ]);
    }
}