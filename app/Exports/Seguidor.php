<?php

namespace App\Exports;

use App\Hseguidores;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;


class seguidor implements FromView
{
    public function view(): View
    {
        return view('exportar.seguidor', [
            'query_seguidor' => Hseguidores::all()
        ]);
    }
}