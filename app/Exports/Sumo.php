<?php

namespace App\Exports;

use App\Hsumo;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;


class sumo implements FromView
{
    public function view(): View
    {
        return view('exportar.sumo', [
            'query_sumo' => Hsumo::all()
        ]);
    }
}
