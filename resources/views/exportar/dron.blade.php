<table>
    <thead>
    <tr>
        <th>id</th>
        <th>Nombre de robot</th>
        <th>Institucion</th>
        <th>Ronda</th>
        <th>NombreEquipo</th>
        <th>NombreCapitan</th>
        <th>Tiempo</th>
        <th>Fecha de creacion</th>
    </tr>
    </thead>
    <tbody>
    @foreach($query_dron as $each_dron)
        <tr>
            <td>{{ $each_dron->Id }}</td>
            <td>{{ $each_dron->NombreRobot }}</td>
            <td>{{ $each_dron->Institucion }}</td>
            <td>{{ $each_dron->Ronda }}</td>
            <td>{{ $each_dron->NombreEquipo }}</td>
            <td>{{ $each_dron->NombreCapitan }}</td>
            <td>{{ $each_dron->Tiempo }}</td>
            <td>{{ $each_dron->updated_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>