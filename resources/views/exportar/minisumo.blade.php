<table>
    <thead>
    <tr>
        <th>id</th>
        <th>Nombre de robot</th>
        <th>Institucion</th>
        <th>Ronda</th>
        <th>NombreEquipo</th>
        <th>NombreCapitan</th>
        <th>Fecha de creacion</th>
    </tr>
    </thead>
    <tbody>
    @foreach($query_minisumo as $each_minisumo)
        <tr>
            <td>{{ $each_minisumo->Id }}</td>
            <td>{{ $each_minisumo->NombreRobot }}</td>
            <td>{{ $each_minisumo->Institucion }}</td>
            <td>{{ $each_minisumo->Ronda }}</td>
            <td>{{ $each_minisumo->NombreEquipo }}</td>
            <td>{{ $each_minisumo->NombreCapitan }}</td>
            <td>{{ $each_minisumo->updated_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
