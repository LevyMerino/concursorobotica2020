<table>
    <thead>
    <tr>
        <th>id</th>
        <th>Nombre de robot</th>
        <th>Institucion</th>
        <th>Ronda</th>
        <th>NombreEquipo</th>
        <th>NombreCapitan</th>
        <th>Tiempo</th>
        <th>Fecha de creacion</th>
    </tr>
    </thead>
    <tbody>
    @foreach($query_seguidor as $each_seguidor)
        <tr>
            <td>{{ $each_seguidor->Id }}</td>
            <td>{{ $each_seguidor->NombreRobot }}</td>
            <td>{{ $each_seguidor->Institucion }}</td>
            <td>{{ $each_seguidor->Ronda }}</td>
            <td>{{ $each_seguidor->NombreEquipo }}</td>
            <td>{{ $each_seguidor->NombreCapitan }}</td>
            <td>{{ $each_seguidor->Tiempo }}</td>
            <td>{{ $each_seguidor->updated_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>