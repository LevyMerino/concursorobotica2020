<table>
    <thead>
    <tr>
        <th>id</th>
        <th>Nombre de robot</th>
        <th>Institucion</th>
        <th>Ronda</th>
        <th>NombreEquipo</th>
        <th>NombreCapitan</th>
        <th>Fecha de creacion</th>
    </tr>
    </thead>
    <tbody>
    @foreach($query_sumo as $each_sumo)
        <tr>
            <td>{{ $each_sumo->Id }}</td>
            <td>{{ $each_sumo->NombreRobot }}</td>
            <td>{{ $each_sumo->Institucion }}</td>
            <td>{{ $each_sumo->Ronda }}</td>
            <td>{{ $each_sumo->NombreEquipo }}</td>
            <td>{{ $each_sumo->NombreCapitan }}</td>
            <td>{{ $each_sumo->updated_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>