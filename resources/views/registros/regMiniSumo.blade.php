@extends('template.layout')

@section('content')
<h2> <p class="text-center">Registro para minisumo</p> </h2>
  <br>


  @if ($errors->any())

<div class="alert alert-danger">

    <strong>Whoops!</strong> There were some problems with your input.<br><br>

    <ul>

        @foreach ($errors->all() as $error)

            <li>{{ $error }}</li>

        @endforeach

    </ul>

</div>

@endif


@if ($message = Session::get('success'))

<div class="alert alert-success">

    <p>{{ $message }}</p>

</div>

@endif

@if ($message = Session::get('register_clone'))

<div class="alert alert-danger">

    <p>{{ $message }}</p>

</div>

@endif

<div class="container-fluid">

 
  <br>

    <form action= "/Minisumos" method="POST" role="form">
    {{ csrf_field() }}
    

<div class="col-sm-3">
<div class="justify-content-center">
     


      <div class="form-group">
        <label for="exampleFormControlSelect1"><h5>Nombre del robot</h5></label>
          <input class="form-control" type="text" placeholder="Nombre del robot" name="NombreRobot">
      </div>

      

      <div class="form-group">
        <label for="exampleFormControlSelect1"><h5>Nombre del capitan</h5></label>
        <input class="form-control" type="text" placeholder="Nombre del capitan" name="NombreCapitan">
      </div>


    <br>

   

      <div class="form-group">
        <label for="exampleFormControlSelect1"><h5>Nombre del equipo</h5></label>
        <input class="form-control" type="text" placeholder="Nombre del robot" name="NombreEquipo">
      </div>
      <div class="form-group">
        <label for="exampleFormControlSelect1"><h5>Nombre de la institucion</h5></label>
        <input list="browsers" name="Institucion">
          <datalist id="browsers">
            <option value="UICUI">
            <option value="TESJO">
            <option value="TESJI">
            <option value="UPA">
            <option value="UAEMEX">
            <option value="ONCA">
          </datalist>
       </div>

    </div>
  </div>
      <div class="col-sm-2">
        <br><br>
        <button type="submit" class="btn btn-success btn-smn btn-block">Registrar</button>
      </div>

   



   
    </form>

</div>

@endsection
